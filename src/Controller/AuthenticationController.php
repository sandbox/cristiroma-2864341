<?php

namespace Drupal\certificate_authentication\Controller;


use Drupal\certificate_authentication\CertificateAuthenticationEvent;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


class AuthenticationController extends ControllerBase {


  public function login(Request $request) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $variables = $request->server->all();
    $identifier = CertificateAuthenticationEvent::getCertificateIdentifier($variables);
    if (!empty($variables[CertificateAuthenticationEvent::SSL_CLIENT_VERIFY])
        && $variables[CertificateAuthenticationEvent::SSL_CLIENT_VERIFY] === 'SUCCESS') {

      \Drupal::logger(__METHOD__)->debug('Certificate authentication started');
      $dispatcher = \Drupal::service('event_dispatcher');
      /** @var CertificateAuthenticationEvent $event */
      $event = $dispatcher->dispatch('certificate_authentication.login', new CertificateAuthenticationEvent($variables));
      if ($user = $event->getAccount()) {
        user_login_finalize($user);
        $ret = $event->getRedirectSuccess();
        \Drupal::logger(__METHOD__)->notice(
          'Authenticated user: %user (%uid) with ID=%id',
          ['%user' => $user->getUsername(), '%uid' => $user->id(), '%id' => $identifier]
        );
      }
      else {
        $ret = $event->getRedirectFailure();
        \Drupal::logger(__METHOD__)->notice('Cannot match any account to this certificate: ID=%id', ['%id' => $identifier]);
      }
    } else {
      // User had valid SSL certificate, but was not authenticated
      drupal_set_message(t("Oops, something went wrong and we could not authenticate you. Please contact technical support, <strong>your identifier</strong>: <pre>%id</pre>", ['%id' => $identifier]), 'error');
    }

    // Avoid broken modules invalid redirection
    if (empty($ret)) {
      $login_form_url = Url::fromRoute('user.login', [], ['absolute' => true])->toString();
      $ret = new RedirectResponse($login_form_url);
    }
    return $ret;
  }
}
