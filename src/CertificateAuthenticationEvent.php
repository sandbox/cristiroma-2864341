<?php

namespace Drupal\certificate_authentication;


use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CertificateAuthenticationEvent extends Event {

  const CERT_DN = 'SSL_CLIENT_S_DN';
  const SSL_CLIENT_VERIFY = 'SSL_CLIENT_VERIFY';
  const SSL_CLIENT_SAN_Email_0 = 'SSL_CLIENT_SAN_Email_0';
  const SSL_CLIENT_SAN_OTHER_msUPN_0 = 'SSL_CLIENT_SAN_OTHER_msUPN_0';
  const SSL_CLIENT_S_DN_CN = 'SSL_CLIENT_S_DN_CN';
  const SSL_CLIENT_S_DN_O = 'SSL_CLIENT_S_DN_O';
  const SSL_CLIENT_M_SERIAL = 'SSL_CLIENT_M_SERIAL';

  /** @var User */
  protected $account;

  protected $sslVariables;

  /** @var RedirectResponse */
  protected $redirectSuccess;

  /** @var RedirectResponse */
  protected $redirectFailure;


  public function __construct(array $sslVariables) {
    $this->sslVariables = $sslVariables;
  }

  /**
   * Retrieve the O SSL certificate attribute (SSL_CLIENT_S_DN_O)
   * @return mixed|null
   */
  public function getCertificateOrganisation() {
    if (!empty($this->sslVariables[self::SSL_CLIENT_S_DN_O])) {
      return $this->sslVariables[self::SSL_CLIENT_S_DN_O];
    }
    return null;
  }

  /**
   * Retrieve the CN SSL certificate attribute (SSL_CLIENT_S_DN_CN)
   * @return mixed|null
   */
  public function getCertificateCommonName() {
    if (!empty($this->sslVariables[self::SSL_CLIENT_S_DN_CN])) {
      return $this->sslVariables[self::SSL_CLIENT_S_DN_CN];
    }
    return null;
  }

  /**
   * Retrieve the email SSL certificate attribute (SSL_CLIENT_SAN_Email_0)
   * @return mixed|null
   */
  public function getCertificateEMail() {
    if (!empty($this->sslVariables[self::SSL_CLIENT_SAN_Email_0])) {
      return $this->sslVariables[self::SSL_CLIENT_SAN_Email_0];
    }
    return null;
  }

  public static function getCertificateIdentifier($variables) {
    $ret = \Drupal::translation()->translate('Request missing mandatory SSL information');
    if (!empty($variables[self::CERT_DN])) {
      $ret = $variables[self::CERT_DN];
    }
    else if (!empty($variables[self::SSL_CLIENT_SAN_Email_0])) {
      $ret = $variables[self::SSL_CLIENT_SAN_Email_0];
    }
    else if (!empty($variables[self::SSL_CLIENT_SAN_OTHER_msUPN_0])) {
      $ret = $variables[self::SSL_CLIENT_SAN_OTHER_msUPN_0];
    }
    else if (!empty($variables[self::SSL_CLIENT_S_DN_CN])) {
      $ret = $variables[self::SSL_CLIENT_S_DN_CN];
    }
    else if (!empty($variables[self::SSL_CLIENT_M_SERIAL])) {
      $ret = $variables[self::SSL_CLIENT_M_SERIAL];
    }
    return $ret;
  }


  /**
   * @return User
   */
  public function getAccount() {
    return $this->account;
  }

  /**
   * @param User $account
   */
  public function setAccount($account) {
    $this->account = $account;
  }

  /**
   * @return RedirectResponse
   */
  public function getRedirectSuccess() {
    return $this->redirectSuccess;
  }

  public function setRedirectSuccess($redirect) {
    $this->redirectSuccess = $redirect;
  }

  /**
   * @return RedirectResponse
   */
  public function getRedirectFailure() {
    return $this->redirectFailure;
  }

  public function setRedirectFailure($redirect) {
    return $this->redirectFailure = $redirect;
  }

  public function getSslVariables() {
    return $this->sslVariables;
  }
}
