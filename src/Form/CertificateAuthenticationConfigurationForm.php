<?php


namespace Drupal\certificate_authentication\Form;




use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CertificateAuthenticationConfigurationForm extends ConfigFormBase {

  const CONFIG_NAME = 'certificate_authentication.settings';
  const GLOBAL_ENABLE = 'global_enable';
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'certificate_authentication_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [ self::CONFIG_NAME, ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(self::CONFIG_NAME);
    $form[self::GLOBAL_ENABLE] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable globally certificate authentication'),
      '#default_value' => $config->get(self::GLOBAL_ENABLE),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);
    $config->set(self::GLOBAL_ENABLE, $form_state->getValue(self::GLOBAL_ENABLE));
    $config->save();

    // Invalidate the authentication form cache to rebuild the form
    $bin = \Drupal::cache('render');
    $ls = \Drupal::languageManager()->getLanguages(\Drupal\Core\Language\LanguageInterface::STATE_CONFIGURABLE);
    foreach ($ls as $ob) {
      $login = \Drupal\Core\Url::fromRoute('user.login', [], ['absolute' => true, 'language' => $ob]);
      $cid = $login->toString() . ':html';
      $bin->invalidate($cid);
    }
    $bin->garbageCollection();
    parent::submitForm($form, $form_state);
  }

}